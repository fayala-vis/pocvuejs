<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UsersController extends Controller
{
    public function index()
    {
        return view('users.fetchUsers');
    }

    public function jquery()
    {
        return view('jquery.fetchUsers');
    }
}
