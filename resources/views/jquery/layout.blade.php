<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="token" id="token" value="{{ csrf_token() }}">
    <title>Crud with vue.js and jquery</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="page-header">
        <h1>
            CRUD with vue.js and jquery <br>
            <small><a href="{{ route('resources') }}">switch to vue-resource</a></small>
        </h1>
    </div>
    <div id="content"></div>
    @yield('content')
</div>
<script src="/js/jquery.min.js"></script>
<script src="/js/vue.js"></script>
@stack('scripts')
</body>
</html>