@extends('layout')
@section('content')
    <div id="UsersController">

        <div class="alert alert-danger" v-if="!isValid">
            <ul>
                <li v-show="!validation.name">El nombre es requerido.</li>
                <li v-show="!validation.email">El formato del email es incorrecto.</li>
                <li v-show="!validation.address">La dirección es requerida.</li>
            </ul>
        </div>

        <form action="#" @submit.prevent="AddNewUser" method="POST" style="padding-top: 2em">
            <div class="form-group">
                <label for="name"> NAME: </label>
                <input type="text" id="name" name="name" v-model="newUser.name" class="form-control">
            </div>

            <div class="form-group">
                <label for="email"> EMAIL: </label>
                <input type="text" id="email" name="email" v-model="newUser.email" class="form-control">
            </div>

            <div class="form-group">
                <label for="address"> ADDRESS: </label>
                <input type="text" id="address" name="address" v-model="newUser.address" class="form-control">
            </div>

            <div class="form-group">
                <button class="btn btn-default" :disabled="!isValid" v-if="!edit" type="submit">Add New User</button>
                <button class="btn btn-default" :disabled="!isValid" v-if="edit" @click.prevent="EditUser(newUser.id)">Edit User</button>
            </div>

        </form>

        <div class="alert alert-success" transition="success" v-if="success">
            Add New User Successful.
        </div>

        <hr>
        <table class="table">
            <thead>
                <th>ID</th>
                <th>NAME</th>
                <th>EMAIL</th>
                <th>ADDRESS</th>
                <th>CREATED_AT</th>
                <th>UPDATED_AT</th>
                <th>CONTROLLERS</th>
            </thead>
            <tbody>
                <tr v-for="user in users">
                    <td>@{{ user.id }}</td>
                    <td>@{{ user.name }}</td>
                    <td>@{{ user.email }}</td>
                    <td>@{{ user.address }}</td>
                    <td>@{{ user.created_at }}</td>
                    <td>@{{ user.updated_at }}</td>
                    <td>
                        <button class="btn btn-sm btn-default" @click.prevent="ShowUser(user.id)">Edit</button>
                        <button class="btn btn-sm btn-danger" @click.prevent="RemoveUser(user.id)">Remove</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection

@push('scripts')
    <script src="/js/script_to_resources.js"></script>

    <style>
        .success-transition {
            transition: all .5 ease-in-out;
        }
        .success-enter, .success-leave{
            opacity: 0;
        }
    </style>
@endpush