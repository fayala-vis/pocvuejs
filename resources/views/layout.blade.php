<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="token" id="token" value="{{ csrf_token() }}">
    <title>Crud with vue.js and vue-resources</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="page-header">
            <h1>
                CRUD with vue.js and vue-resources <br>
                <small><a href="{{ route('jquery') }}">switch to jquery</a></small>
            </h1>
        </div>
        @yield('content')
    </div>
    <script src="/js/vendor.js"></script>
    @stack('scripts')
</body>
</html>