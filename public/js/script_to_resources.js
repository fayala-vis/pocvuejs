var emailRE = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

var vm = new Vue({
    http: {
        root: '/root',
        headers: {
            'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
        }
    },
    el: "#UsersController",
    methods: {
      fetchUsers: function()
      {
        var self = this;
        this.$http.get('/api/users').then(
            function(response)
            {
                // response vuelve en formato resources, nuestra data viene dentro del elemento body
                self.users = response.body;
            }
        );
      },
        AddNewUser: function()
        {
            var self = this;
            var user = this.newUser;

            this.newUser = {name:"", email:"", address:""};

            this.$http.post('/api/users', user).then(
                function()
                {
                    self.success = true;
                    self.fetchUsers();

                    setTimeout(function(){
                        self.success = false;
                    },5000);
                }
            )
        },

        ShowUser: function(id)
        {
            self = this;
            this.edit = true;
            this.$http.get('/api/users/' + id).then(
                function(response)
                {
                    user = response.body;
                    self.newUser.id = user.id;
                    self.newUser.name = user.name;
                    self.newUser.email = user.email;
                    self.newUser.address = user.address;
                }
            );
        },
        EditUser: function(id)
        {
            var user = this.newUser;
            var self = this;
            this.$http.patch('/api/users/' + id, user).then(
                function(response){
                    console.log(response.body);
                    self.newUser = {id: "", name: "", email: "", address: ""};
                    self.edit = false;
                    self.fetchUsers();
                }
            );

        },
        RemoveUser: function(id)
        {
            ConfirmBox = confirm("Are you sure, you want to delete this User?");
            var self = this;
            if(ConfirmBox) {
                this.$http.delete('/api/users/' + id).then(
                    function (response){
                        self.fetchUsers();
                    });
            }
        }

    },
    data: {
        users: {},
        newUser: {
            id: "",
            name: "",
            email: "",
            address: ""
        },
        success: false,
        edit: false
    },
    computed: {
        validation: function()
        {
            return {
                name: !! this.newUser.name.trim(), // !! vuelve boolean la respuesta
                email: emailRE.test(this.newUser.email), // se utilizan regex
                address: !!this.newUser.address.trim() // el trim remueve espacios, si no hay nada devuelve false
            }
        },
        isValid: function()
        {
            var validation = this.validation;


            //Object.keys(obj) = returns an array whose elements are string corresponding to the enumerable properies found directly upon object.
            // regresa un array con las posiciones (keys) de un objeto.

            var keys = Object.keys(validation);
            console.log('Object.keys(obj) ', keys);

            // [].every( callback ) = if callback returned a true value for all elements, every will return true
            // la funcion every regresará true si cada una de las peticiones que se harán por posición responden true.
            var isValid = keys.every(function(key){
                return validation[key];
            });
            console.log('[].every( callback )', isValid);

            return isValid;
        }
    },
    mounted: function()
    {
        // para la versión vue.js 2 se utiliza mounted en vez de ready.
        this.fetchUsers();
    }
});