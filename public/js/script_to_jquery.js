var emailRE = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

Vue.filter('reverse', function(item){

});

var vm = new Vue({
    el: "#UsersController",
    methods: {
        fetchUsers: function()
        {
            var self = this;
            $.ajax({
                type: "GET",
                url: "/api/users"
            }).done(function(users){
                self.users = users;
            });
        },
        AddNewUser: function()
        {
            var self = this;
            var user = this.newUser;

            this.newUser = {name:"", email:"", address:""};

            $.ajax({
                type: "POST",
                url: "/api/users",
                data: user
            }).done(function(){
               self.success = true;
                self.fetchUsers();

                setTimeOut(function(){
                   self.success = false;
                }, 5000);
            });
        },

        ShowUser: function(id)
        {
            self = this;
            this.edit = true;

            $.ajax({
               type: "GET",
                url: "/api/users/" + id
            }).done(function(user){
                self.newUser.id = user.id;
                self.newUser.name = user.name;
                self.newUser.email = user.email;
                self.newUser.address = user.address;
            });
        },
        EditUser: function(id)
        {
            var user = this.newUser;
            var self = this;

            $.ajax({
                type: "PATCH",
                url: "/api/users/" + id,
                data: user
            }).done(function(){
                self.newUser = {id: "", name: "", email: "", address: ""};
                self.edit = false;
                self.fetchUsers();
            });

        },
        RemoveUser: function(id)
        {
            ConfirmBox = confirm("Are you sure, you want to delete this User?");
            var self = this;
            if(ConfirmBox) {
                $.ajax({
                    type: "DELETE",
                    url: "/api/users/" + id
                }).done(function(){
                    self.fetchUsers();
                });
            }
        }

    },
    data: {
        users: {},
        newUser: {
            id: "",
            name: "",
            email: "",
            address: ""
        },
        success: false,
        edit: false
    },
    computed: {
        full_name: function()
        {
            return this.first_name + " " + this.last_name;
        },
        validation: function()
        {
            return {
                name: !! this.newUser.name.trim(), // !! vuelve boolean la respuesta
                email: emailRE.test(this.newUser.email), // se utilizan regex
                address: !!this.newUser.address.trim() // el trim remueve espacios, si no hay nada devuelve false
            }
        },
        isValid: function()
        {
            var validation = this.validation;


            //Object.keys(obj) = returns an array whose elements are string corresponding to the enumerable properies found directly upon object.
            // regresa un array con las posiciones (keys) de un objeto.

            var keys = Object.keys(validation);
            console.log('Object.keys(obj) ', keys);

            // [].every( callback ) = if callback returned a true value for all elements, every will return true
            // la funcion every regresará true si cada una de las peticiones que se harán por posición responden true.
            var isValid = keys.every(function(key){
                return validation[key];
            });
            console.log('[].every( callback )', isValid);

            return isValid;
        }
    },
    mounted: function()
    {
        // para la versión vue.js 2 se utiliza mounted en vez de ready.
        this.fetchUsers();

        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')}
        });
    }
});